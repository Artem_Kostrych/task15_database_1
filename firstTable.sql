-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`family_tree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`family_tree` (
  `id` INT GENERATED ALWAYS AS () VIRTUAL,
  `name` VARCHAR(20) NOT NULL,
  `surname` VARCHAR(20) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `date_of_death` DATE NOT NULL,
  `place_of_birth` VARCHAR(45) NULL,
  `place_of_death` VARCHAR(45) NULL,
  `credit_card_number` VARCHAR(45) NULL,
  `family_treecol` VARCHAR(16) NULL,
  `family_tree_id` INT NOT NULL,
  PRIMARY KEY (`id`, `family_tree_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_family_tree_family_tree1_idx` (`family_tree_id` ASC) VISIBLE,
  CONSTRAINT `fk_family_tree_family_tree1`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `mydb`.`family_tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`family_values`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`family_values` (
  `name` VARCHAR(40) NOT NULL,
  `estimated_cost` INT NULL,
  `maximum_cost` INT NULL,
  `minimum_cost` INT NULL,
  `code_in_catalog` INT NOT NULL,
  PRIMARY KEY (`code_in_catalog`),
  UNIQUE INDEX `code_in_catalog_UNIQUE` (`code_in_catalog` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`family_tree_has_family_values`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`family_tree_has_family_values` (
  `family_tree_id` INT NOT NULL,
  `family_values_code_in_catalog` INT NOT NULL,
  PRIMARY KEY (`family_tree_id`, `family_values_code_in_catalog`),
  INDEX `fk_family_tree_has_family_values_family_values1_idx` (`family_values_code_in_catalog` ASC) VISIBLE,
  INDEX `fk_family_tree_has_family_values_family_tree_idx` (`family_tree_id` ASC) VISIBLE,
  CONSTRAINT `fk_family_tree_has_family_values_family_tree`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `mydb`.`family_tree` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_tree_has_family_values_family_values1`
    FOREIGN KEY (`family_values_code_in_catalog`)
    REFERENCES `mydb`.`family_values` (`code_in_catalog`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`sex`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`sex` (
  `sex` VARCHAR(10) NULL,
  `id` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`family_tree_copy1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`family_tree_copy1` (
  `id` INT GENERATED ALWAYS AS () VIRTUAL,
  `name` VARCHAR(20) NOT NULL,
  `surname` VARCHAR(20) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `date_of_death` DATE NOT NULL,
  `place_of_birth` VARCHAR(45) NULL,
  `place_of_death` VARCHAR(45) NULL,
  `credit_card_number` VARCHAR(45) NULL,
  `family_treecol` VARCHAR(16) NULL,
  `family_tree_id` INT NOT NULL,
  `sex_id` INT NOT NULL,
  PRIMARY KEY (`id`, `family_tree_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_family_tree_copy1_sex1_idx` (`sex_id` ASC) VISIBLE,
  CONSTRAINT `fk_family_tree_copy1_sex1`
    FOREIGN KEY (`sex_id`)
    REFERENCES `mydb`.`sex` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`family_satellite`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`family_satellite` (
  `id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(20) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `date_of_death` DATE NOT NULL,
  `place_of_birth` VARCHAR(45) NOT NULL,
  `place_of_death` VARCHAR(45) NOT NULL,
  `date_of_marriage` DATE NOT NULL,
  `family_tree_copy1_id` INT NOT NULL,
  `family_tree_copy1_family_tree_id` INT NOT NULL,
  `sex_id` INT NOT NULL,
  PRIMARY KEY (`id`, `family_tree_copy1_id`, `family_tree_copy1_family_tree_id`),
  INDEX `fk_family_satellite_family_tree_copy11_idx` (`family_tree_copy1_id` ASC, `family_tree_copy1_family_tree_id` ASC) VISIBLE,
  INDEX `fk_family_satellite_sex1_idx` (`sex_id` ASC) VISIBLE,
  CONSTRAINT `fk_family_satellite_family_tree_copy11`
    FOREIGN KEY (`family_tree_copy1_id` , `family_tree_copy1_family_tree_id`)
    REFERENCES `mydb`.`family_tree_copy1` (`id` , `family_tree_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_family_satellite_sex1`
    FOREIGN KEY (`sex_id`)
    REFERENCES `mydb`.`sex` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
